#!/bin/bash

host=192.168.1.1
confdir=$(mktemp -d)
cp -rL $1/* $confdir

function ssh () {
  /usr/bin/ssh -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" "$@"
}

function scp () {
  /usr/bin/scp -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" "$@"
}


echo "Telnet: run passwd and /etc/init.d/dropbear start"
telnet $host

echo "Edit to assign wireless channel and txpower"
sensible-editor $confdir/etc/config/wireless

echo "Push configuration to device"
scp -r $confdir/* root@$host:/

echo "Disable Firewall"
ssh root@$host /etc/init.d/firewall disable

echo "Disable dnsmasq"
ssh root@$host /etc/init.d/dnsmasq disable

echo "Rebooting device"
ssh root@$host /sbin/reboot
