/* LABELS.MSS CONTENTS:
 * - place names
 * - area labels
 * - waterway labels 
 */

/* Font sets are defined in palette.mss */


/* ================================================================== */
/* AREA LABELS
/* ================================================================== */

#area_label[type='golf_course'][zoom>=10],
#area_label[type='park'][zoom>=10] {
  text-name: "''";
  text-fill: @park * 0.6;
  text-halo-fill: fadeout(@park,25%);
  text-halo-radius: 2;
  text-face-name:@sans;
  text-wrap-width:30;
  [zoom=10][area>102400000],
  [zoom=11][area>25600000],
  [zoom=13][area>1600000],
  [zoom=14][area>320000],
  [zoom=15][area>80000],
  [zoom=16][area>20000],
  [zoom=17][area>5000],
  [zoom>=18][area>=0] {
    text-name: "[name]";
  }
  [zoom>=15][area>1600000],
  [zoom>=16][area>80000],
  [zoom>=17][area>20000],
  [zoom>=18][area>5000] {
    text-name: "[name]";
    text-size: 12;
    text-line-spacing: 2;
    text-wrap-width: 60;
  }
  [zoom=16][area>1600000],
  [zoom=17][area>80000],
  [zoom>=18][area>20000] {
    text-size: 14;
    text-line-spacing: 4;
    text-character-spacing: 1;
    text-wrap-width: 120;
  }
  [zoom>=17][area>1600000],
  [zoom>=18][area>80000] {
    text-size: 16;
    text-line-spacing: 6;
    text-character-spacing: 2;
    text-wrap-width: 180;
  }
}

#area_label[type='cemetery'][zoom>=10] {
  text-name: "''";
  text-fill: @cemetery * 0.6;
  text-halo-fill: fadeout(@cemetery,25%);
  text-halo-radius: 2;
  text-face-name:@sans;
  text-wrap-width:30;
  [zoom=10][area>102400000],
  [zoom=11][area>25600000],
  [zoom=13][area>1600000],
  [zoom=14][area>320000],
  [zoom=15][area>80000],
  [zoom=16][area>20000],
  [zoom=17][area>5000],
  [zoom>=18][area>=0] {
    text-name: "[name]";
  }
  [zoom=15][area>1600000],
  [zoom=16][area>80000],
  [zoom=17][area>20000],
  [zoom>=18][area>5000] {
    text-name: "[name]";
    text-size: 12;
    text-line-spacing: 2;
    text-wrap-width: 60;
  }
  [zoom=16][area>1600000],
  [zoom=17][area>80000],
  [zoom>=18][area>20000] {
    text-size: 14;
    text-line-spacing: 4;
    text-character-spacing: 1;
    text-wrap-width: 120;
  }
  [zoom>=17][area>1600000],
  [zoom>=18][area>80000] {
    text-size: 16;
    text-line-spacing: 6;
    text-character-spacing: 2;
    text-wrap-width: 180;
  }
}

#area_label[type='hospital'][zoom>=10] {
  text-name: "''";
  text-fill: @hospital * 0.6;
  text-halo-fill: fadeout(lighten(@hospital,10%),50%);
  text-halo-radius: 2;
  text-face-name:@sans;
  text-wrap-width:30;
  [zoom=14][area>80000],
  [zoom=15][area>80000],
  [zoom=16][area>20000],
  [zoom=17][area>5000],
  [zoom>=18][area>=0] {
    text-name: "[name]";
    text-size: 12;
    text-line-spacing: 2;
    text-wrap-width: 60;
  }
  [zoom=16][area>80000],
  [zoom=17][area>20000],
  [zoom>=18][area>5000] {
    text-name: "[name]";
    text-size: 12;
    text-line-spacing: 2;
    text-wrap-width: 60;
  }
  [zoom=17][area>80000],
  [zoom>=18][area>20000] {
    text-size: 14;
    text-line-spacing: 4;
    text-character-spacing: 1;
    text-wrap-width: 120;
  }
  [zoom>=18][area>80000] {
    text-size: 16;
    text-line-spacing: 6;
    text-character-spacing: 2;
    text-wrap-width: 180;
  }
}

#area_label[type='college'][zoom>=17],
#area_label[type='school'][zoom>=17],
#area_label[type='university'][zoom>=17] {
  text-name: "''";
  text-fill: #777;
  text-halo-fill: fadeout(lighten(@school,10%),50%);
  text-halo-radius: 2;
  text-face-name:@sans;
  text-wrap-width:50;
  text-size: 11;
  [zoom=13][area>80000],
  [zoom=14][area>80000],
  [zoom=14][area>80000],
  [zoom=15][area>80000] {
    text-name: "[name]";
    text-size: 12;
  }
  [zoom=16][area>20000],
  [zoom=17][area>5000],
  [zoom>=17][area>=0] {
  }
  [zoom=16][area>80000],
  [zoom=17][area>20000],
  [zoom>=17][area>5000] {
    text-name: "[name]";
    text-size: 12;
    text-line-spacing: 2;
    text-wrap-width: 60;
  }
  [zoom=18][area>80000],
  [zoom>=18][area>20000] {
    text-size: 0;
    text-line-spacing: 4;
    text-character-spacing: 1;
    text-wrap-width: 120;
  }
}

#area_label[type='water'][zoom>=10] {
  text-name: "''";
  text-fill: @water * 0.75;
  text-halo-fill: fadeout(lighten(@water,5%),25%);
  text-halo-radius: 1;
  text-face-name:@sans_italic;
  text-wrap-width:30;
  [zoom=10][area>102400000],
  [zoom=11][area>25600000],
  [zoom=13][area>1600000],
  [zoom=14][area>320000],
  [zoom=15][area>80000],
  [zoom=16][area>20000],
  [zoom=17][area>5000],
  [zoom=18][area>=0] {
    text-name: "[name]";
  }
  [zoom=15][area>1600000],
  [zoom=16][area>80000],
  [zoom=17][area>20000],
  [zoom=18][area>5000] {
    text-name: "[name]";
    text-size: 12;
    text-line-spacing: 2;
    text-wrap-width: 60;
    text-character-spacing: 1;
  }
  [zoom=16][area>1600000],
  [zoom=17][area>80000],
  [zoom=18][area>20000] {
    text-size: 14;
    text-line-spacing: 4;
    text-character-spacing: 2;
    text-wrap-width: 120;
  }
  [zoom>=17][area>1600000],
  [zoom>=18][area>80000] {
    text-size: 16;
    text-line-spacing: 6;
    text-character-spacing: 3;
    text-wrap-width: 180;
  }
}
   
#poi[type='university'][zoom>=17],
#poi[type='hospital'][zoom>=16],
#poi[type='school'][zoom>=17],
#poi[type='library'][zoom>=17] {
  text-name:"[name]";
  text-face-name:@sans;
  text-size:10;
  text-wrap-width:30;
  text-fill: @poi_text;
}


/* ================================================================== */
/* WATERWAY LABELS
/* ================================================================== */

#waterway_label[type='river'][zoom>=13],
#waterway_label[type='canal'][zoom>=15],
#waterway_label[type='stream'][zoom>=17] {
  text-name: '[name]';
  text-face-name: @sans_italic;
  text-fill: @water * 0.75;
  text-halo-fill: fadeout(lighten(@water,5%),25%);
  text-halo-radius: 1;
  text-placement: line;
  text-min-distance: 400;
  text-size: 10;
  [type='river'][zoom=14],
  [type='canal'][zoom=16],
  [type='stream'][zoom=18] {
    text-name: "[name].replace('([\S\ ])','$1 ')";
  }
  [type='river'][zoom=15],
  [type='canal'][zoom=17] {
    text-size: 11;
    text-name: "[name].replace('([\S\ ])','$1 ')";
  }
  [type='river'][zoom>=16],
  [type='canal'][zoom=18] {
    text-size: 14;
    text-name: "[name].replace('([\S\ ])','$1 ')";
    text-spacing: 300;
  }
}

/* ================================================================== */
/* ROAD LABELS
/* ================================================================== */


#mainroad_label[type='primary'][zoom>17],
#mainroad_label[type='secondary'][zoom>17],
#mainroad_label[type='tertiary'][zoom>17] {
  text-name:'[name]';
  text-face-name:@sans;
  text-placement:line;
  text-fill:@road_text;
  text-halo-fill:@road_halo;
  text-halo-radius:1;
  text-min-distance:60;
  text-size:11;
}

#minorroad_label[zoom>17] {
  text-name:'[name]';
  text-face-name:@sans;
  text-placement:line;
  text-size:9;
  text-fill:@road_text;
  text-halo-fill:@road_halo;
  text-halo-radius:1;
  text-min-distance:60;
  text-size:11;
}

/* ================================================================== */
/* ONE-WAY ARROWS
/* ================================================================== */

#motorway_label[oneway!=0][zoom>=18],
#mainroad_label[oneway!=0][zoom>=18],
#minorroad_label[oneway!=0][zoom>=18] {
  marker-placement:line;
  marker-max-error: 0.5;
  marker-spacing: 200;
  marker-file: url(img/icon/oneway.svg);
  [oneway=-1] { marker-file: url(img/icon/oneway-reverse.svg); }
  [zoom=16] { marker-transform: "scale(0.5)"; }
  [zoom=17] { marker-transform: "scale(0.75)"; }
}


/* ================================================================== */
/* AMENITIES
/* ================================================================== */


#poi[type='hospital'][zoom>=13] {
  point-file: url(debmaki/renders/hospital-18.png);
  text-face-name:@sans_bold;
  text-size:9;
  text-wrap-width:30;
  text-fill: @poi_text;
  text-name:"[name]";    
}

#poi[type= 'fuel'][zoom>=17] {
  point-file: url(debmaki/renders/fuel-12.png);
}

#poi {
  text-dx: 8;
  text-face-name:@sans_bold;
  text-fill: #000; /* TODO: remove */
  text-halo-fill:@place_halo;
  text-halo-radius:1;
  text-line-spacing: 1;
  text-name:"''";
  text-size:10;
  text-wrap-width:70;
  
  [type='mall'][zoom>=16] {
    text-size:12;
    text-fill: @poi_text;
    text-name: "[name]";
  }
  [type='hotel'][recommended='TRUE'][zoom>=15] {
    text-dx: 0;
    text-dy: 13;
    point-file: url(debmaki/renders/lodging-22.png);
    text-name: "[name]";
  }
  
  [type='hotel'][recommended='TRUE'][zoom>=16] {
    text-dx: 0;
    text-dy: 25;
    point-file: url(debmaki/renders/lodging-40.png);
    text-fill: #000;
    text-name: "[name]";
    
  }
  
  
  [type='hotel'][zoom>=16] {
    text-dx: 0;
    text-dy: 10;
    point-file: url(debmaki/renders/lodging-18.png);
    text-fill: @poi_text;
    text-name: "[name]";
  }


  [type='hostel'][zoom>=17] {
    text-dx: 0;
    text-dy: 10;
    point-file: url(debmaki/renders/hostel-18.png);
    text-fill: @poi_text;
    text-name: "[name]";
  }

  [type='restaurant'][zoom>=16] {
    text-dx: 0;
    text-dy: 10;
    point-file: url(debmaki/renders/restaurant-18.png);
    text-fill: @poi_text;
    text-name: "[name]";

  }

  [type='dance'][zoom>=16] {
    text-dx: 0;
    text-dy: 10;
    point-file: url(debmaki/renders/dance-18.png);
    text-fill: @poi_text;
    text-name: "[name]";
  }

  [type='bank'][zoom>=16] {
    point-file: url(debmaki/renders/money-18.png);
  }

  [type='atm'][zoom>=16] {
   point-file: url(debmaki/renders/money-18.png);
  }

  [type='bar'][zoom>=16] {
    text-dx: 0;
    text-dy: 9;
    point-file: url(debmaki/renders/bar-18.png);
    text-fill: @poi_text;
    text-name: "[name]";
  }

  [type='cafe'][zoom>=16] {
   point-file: url(debmaki/renders/cafe-18.png);
  }

  [type='store'][zoom>=16] {
   point-file: url(debmaki/renders/shop-12.png);
  }

  [type='supermarket'][zoom>=16] {
    point-file: url(debmaki/renders/grocery-18.png);
  }

  [type='fast_food'][zoom>=16],
  [type='deli'][zoom>=16] {
    point-file: url(debmaki/renders/fast-food-12.png);
  }

  [type='airport'][zoom>=13] {
    text-dx: 0;
    text-dy: 20;
    point-file: url(debmaki/renders/airport-24.png);
    text-size:18;
    text-wrap-width: 120;
    text-fill: @poi_text;
    text-name: "[name]";
  }
  
  [type='airport'][zoom=15] {
    text-dx: 0;
    text-dy: 20;
    point-file: url(debmaki/renders/airport-24.png);
    text-size:14;
    text-wrap-width: 70;
    text-fill: @poi_text;
    text-name: "[name]"; 
  }

  [type='taxi'][zoom>=13] {
    point-file: url(debmaki/renders/taxi-yellow-18.png);

    [recommended="TRUE"][zoom>=13] {
      point-file: url(debmaki/renders/taxi-blue-18.png);
    }
  }
  
  [type='taxi'][zoom>=16] {
    text-dx: 0;
    text-dy: 12;
    point-file: url(debmaki/renders/taxi-yellow-24.png);
    text-fill: @poi_text;
    text-name: "[name]";
    text-wrap-width:40;

    [recommended="TRUE"][zoom>=16] {
      text-dy: 12;
      point-file: url(debmaki/renders/taxi-blue-24.png);
      text-fill: @poi_text;
      text-name: "[name]";
    }
  }

  [type='pharmacy'][zoom>=16] {
   point-file: url(debmaki/renders/pharmacy-18.png);
  }

  [type='venue'][zoom>=13][zoom<=16] {
    point-file: url(debmaki/renders/debconf.png);
  }

  [type='hacklab'][zoom>=17] {
    text-dx: 0;
    text-dy: 21;
    point-file: url(debmaki/renders/hacklab-42.png);
    text-name: "[name]";
    text-wrap-width: 40;
  }

  [type='hacklab'][zoom>=18] {
    text-dx: 0;
    text-dy: 41;
    point-file: url(debmaki/renders/hacklab-76.png);
    text-name: "[name]";
    text-wrap-width: 40;
  }

  [type='talkroom'][zoom>=17] {
    text-dx: 0;
    text-dy: 22;
    point-file: url(debmaki/renders/talkroom-42.png);
    text-name: "[name]";
  }

[type='talkroom'][zoom>=18] {
    text-dx: 0;
    text-dy: 40;
    point-file: url(debmaki/renders/talkroom-76.png);
    text-name: "[name]";
  }

  [type='Bof_room'][zoom>=17] {
    text-dx: 0;
    text-dy: 21;
    point-file: url(debmaki/renders/bof-42.png);
    text-name: "[name]";
  }
[type='Bof_room'][zoom>=18] {
    text-dx: 0;
    text-dy: 40;
    point-file: url(debmaki/renders/bof-76.png);
    text-name: "[name]";
  }

  [type='food_area'][zoom>=17] {
    text-dx: 0;
    text-dy: 22;
    point-file: url(debmaki/renders/food-42.png);
    text-name: "[name]";
  }
  [type='food_area'][zoom>=18] {
    text-dx: 0;
    text-dy: 40;
    point-file: url(debmaki/renders/food-76.png);
    text-name: "[name]";
  }
  [type='caw_party'][zoom>=17] {
    text-dx: 0;
    text-dy: 25;
    point-file: url(debmaki/renders/caw-50.png);
    text-name: "[name]";
  }
  [type='concert'][zoom>=17] {
    text-dx: 0;
    text-dy: 25;
    point-file: url(debmaki/renders/concert-50.png);
    text-name: "[name]";
  }
}





/* ****************************************************************** */